## Sobre la llicència d’aquest *Sobre l’intent de crear un registre jove en català: de* Cul-de-sac *a TV3*

Sens prejudici dels drets que puguin correspondre a Francesc Bernat i Baltrons, Pere MiG ha renunciat a tots els drets d’autor i els drets connexos o afins a *Sobre l’intent de crear un registre jove en català:de* Cul-de-sac *a TV3*, tot oferint-lo sota domini públic.

