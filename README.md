# README
Edició d'un article de recerca universitari elaborat en coautoria el 1986 i presentat com a treball de fi de curs per a *Sociolingüística catalana*, matèria impartida per Lluís V. Aracil. 

En aquest treball s'analitza l’últim intent que, d’abast general, s’ha fet en el camp del còmic d'avantguarda en català, *Cul-de-sac* (15 números apareguts del 23 d'abril a 29 de juliol de 1982). Alhora que s'intenta de veure què va significar en el seu context i com havia variat aquest context, en el qual aquesta publicació fou un exemple a tenir en compte.
